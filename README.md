Projekt zaliczeniowy z przedmiotu "Systemy Zarządzania Treścią"

Projekt systemu CMS napisany przy użyciu framework'a Django 1.8 w środowisku Python 3.6.


Repozytorium: [https://bitbucket.org/Kysio/put-cms/](https://bitbucket.org/Kysio/put-cms/)


Krzysztof Kozłowski

inf108589


## Moduły

Blog - moduł wyświetlający artykuły

Shop - moduł sklepu, wyświetlanie listy produktów oraz dodawanie ich do koszyka ( brak możliwości składania zamówienia )

## Konto administratora

Superuser:
user: *admin*
pasw: *bezhasla*

## Struktura systemu CMS

System CMS bazuje na frameworku Django 1.8, który dostarcza gotowe rozwiązania pozwalające na tworzenie kont użytkowników/administratorów wraz z odpowiednimi poziomami uprawnień realizowanych przy pomocy grup użytkowników.

Moduł "Blog" dodaje możliwość tworzenia treści tekstowych i ich publikacji w publicznej części systemu. Treść składa się z Tytułu, treści poddającej się podstawowemu formatowaniu, autora oraz daty utworzenia.

Moduł "Shop" dostarcza podstawowe mechanizmy tworzenia produktów oraz kategorii produktów. Produkty składają się z nazwy, ceny, krótkiego opisu, ilości i grafiki. Mogą zostać przydzielone do jednej kategorii.

## Uruchamianie

### Wymagania

Python ^3.6.0
Django ^1.8.1
Pillow ^4.0.0

## Schemat bazy danych

### Admin

```
TABLE "auth_permission"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"name" varchar(50) NOT NULL
"content_type_id" integer NOT NULL REFERENCES "django_content_type" ("id")
"codename" varchar(100) NOT NULL
UNIQUE ("content_type_id", "codename")
```

```
TABLE "auth_group"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"name" varchar(80) NOT NULL UNIQUE
```

```
TABLE "auth_group_permissions"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"group_id" integer NOT NULL REFERENCES "auth_group" ("id")
"permission_id" integer NOT NULL REFERENCES "auth_permission" ("id")
UNIQUE ("group_id", "permission_id")
```

```
TABLE "auth_user"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"password" varchar(128) NOT NULL
"last_login" datetime NOT NULL
"is_superuser" bool NOT NULL
"username" varchar(30) NOT NULL UNIQUE
"first_name" varchar(30) NOT NULL
"last_name" varchar(30) NOT NULL
"email" varchar(75) NOT NULL
"is_staff" bool NOT NULL
"is_active" bool NOT NULL
"date_joined" datetime NOT NULL
```

```
TABLE "auth_user_groups"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"user_id" integer NOT NULL REFERENCES "auth_user" ("id")
"group_id" integer NOT NULL REFERENCES "auth_group" ("id")
UNIQUE ("user_id", "group_id")
```

```
TABLE "auth_user_user_permissions"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"user_id" integer NOT NULL REFERENCES "auth_user" ("id")
"permission_id" integer NOT NULL REFERENCES "auth_permission" ("id")
UNIQUE ("user_id", "permission_id")
```

```
TABLE "django_admin_log"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"action_time" datetime NOT NULL
"object_id" text NULL
"object_repr" varchar(200) NOT NULL
"action_flag" smallint unsigned NOT NULL
"change_message" text NOT NULL
"content_type_id" integer NULL REFERENCES "django_content_type" ("id"), "user_id" integer NOT NULL REFERENCES "auth_user" ("id")
```

### Blog

```
TABLE "blog_post"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"title" varchar(250) NOT NULL
"slug" varchar(255) NOT NULL
"body" text NOT NULL
"publish" datetime NOT NULL
"created" datetime NOT NULL
"updated" datetime NOT NULL
"status" varchar(10) NOT NULL
"author_id" integer NOT NULL REFERENCES "auth_user" ("id")
```

### Shop

```
TABLE "shop_category"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"name" varchar(200) NOT NULL
"slug" varchar(200) NOT NULL UNIQUE
```

```
TABLE "shop_product"
"id" integer NOT NULL PRIMARY KEY AUTOINCREMENT
"name" varchar(200) NOT NULL
"slug" varchar(200) NOT NULL
"image" varchar(100) NOT NULL
"description" text NOT NULL
"price" decimal NOT NULL
"stock" integer unsigned NOT NULL
"avaiable" bool NOT NULL
"created" datetime NOT NULL
"updated" datetime NOT NULL
"category_id" integer NOT NULL REFERENCES "shop_category" ("id")
```

